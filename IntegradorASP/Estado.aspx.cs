﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegradorASP
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string Mensaje = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //Agregar PostBack
            if(IsPostBack)
            {
                lblPostBack.Text = "Es postBack";
            }
            else
            {
                lblPostBack.Text = "No es postBack";
            }

            //
            //WebConfig
            lblWebConfig.Text = "valor de la clave empresa: " + System.Configuration.ConfigurationManager.AppSettings["empresa"];
            
            //cookies
            if(Request.Cookies["Sesion"] !=null)
            {
                Mensaje = Mensaje + "Valor de la cookie de sesión: " + Request.Cookies["Sesion"].Value + "<br>";
            }
            if(Request.Cookies["Persistentes"]!=null)
            {
                Mensaje = Mensaje + "Valor de la cookie persistente: " + Request.Cookies["Persistentes"].Value + "<br>";
            }
            if(Request.Cookies["PersistenteMultivalor"]!=null)
            {
                Mensaje = Mensaje + "Valor de la cookie persistente multivalor correspondiente a la fecha: " + Request.Cookies["PersistenteMultivalor"]["Fecha"] + "<br>";
                Mensaje = Mensaje + "Valor de la cookie persistente multivalor correspondiente a la hora: " + Request.Cookies["PersistenteMultivalor"]["Hora"] + "<br>";
            }
            this.lblCookies.Text = Mensaje;   
            
            //ViewConfig
            if(ViewState["Persona"] !=null)
            {
                Persona obj = (Persona)ViewState["Persona"];
                this.lblViewState.Text = obj.Nombre;
            }

            // Session y Application
            if (Session["VariableSession"] != null)
            {
                Persona obj = (Persona)Session["VariableSession"];
                this.lblVar.Text = obj.Nombre;
            }

            if (Application["VariableApplication"] != null)
            {
                Persona obj = (Persona)Application["VariableApplication"];
                this.lblVar2.Text = obj.Nombre;
            }
        }

        //cookies
        protected void btnCookie1_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["Sesion"] == null)
            {
                Response.Cookies["Sesion"].Value = DateTime.Now.ToString();
            }
        }

        protected void btnCookie2_Click(object sender, EventArgs e)
        {
            if(Request.Cookies["Persistentes"]==null)
            {
                Response.Cookies["Persistentes"].Value = DateTime.Now.ToString();
                Response.Cookies["Persistentes"].Expires = DateTime.Now.AddYears(1);
            }

            if(Request.Cookies["PersistenteMultivalor"]==null)
            {
                Response.Cookies["PersistenteMultivalor"]["Fecha"] = DateTime.Now.Date.ToString();
                Response.Cookies["PersistenteMultivalor"]["Hora"] = DateTime.Now.Hour.ToString();
                Response.Cookies["PersistenteMultivalor"].Expires = DateTime.Now.AddYears(1);
            }
        }

        protected void btnCookie3_Click(object sender, EventArgs e)
        {
            Response.Cookies["Persistentes"].Expires = DateTime.Now.AddYears(-1);
            Response.Cookies["PersistenteMultivalor"].Expires = DateTime.Now.AddYears(-1);
        }

        protected void ButtonRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void btnViewState_Click(object sender, EventArgs e)
        {
            if(ViewState["Persona"] == null)
            {
                Persona obj = new Persona("juan");
                ViewState["Persona"] = obj;
            }
        }

        protected void btnVar1_Click(object sender, EventArgs e)
        {
            Persona obj = new Persona("pedro");
            Session["VariableSession"] = obj;
        }

        protected void btnVar2_Click(object sender, EventArgs e)
        {
            Persona obj = new Persona("lopez");
            Application["VariableApplication"] = obj;
        }

        [Serializable]
        public class Persona
        {
            public Persona() { }
            public Persona(string ParametroNombre)
            {
                this.Nombre = ParametroNombre;
            }
            public string Nombre { get; set; }

        }
    }
}