﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Estado.aspx.cs" Inherits="IntegradorASP.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel-primary">
        <div class="panel-heading">
            ejemplo de mantenimineto de estado
        </div>

        <div class="panel-body">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Postback
                </div>
                <div class="panel-body">
                    <asp:Label id="lblPostBack" Text="" runat="server" ></asp:Label>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Web config
                </div>
                <div class="panel-body">
                    <asp:Label id="lblWebConfig" Text="" runat="server" ></asp:Label>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Cookies
                </div>
                <div class="panel-body">
                    <asp:Label id="lblCookies" Text="" runat="server" />
                </div>
                <div class="panel panel-footer">
                    <asp:Button id="btnCookie1" class="btn btn-default" Text="Crear cookie de sesión" runat="server" onClick="btnCookie1_Click"/>
                    <asp:Button id="btnCookie2" class="btn btn-default" Text="Crear cookies persistentes" runat="server" onClick="btnCookie2_Click"/>
                    <asp:Button id="btnCookie3" class="btn btn-default" Text="Borrar cookies persistentes" runat="server" onClick="btnCookie3_Click"/>
                    <asp:Button id="btnCookie4" class="btn btn-default" Text="postBack" runat="server"/>
                    <asp:Button id="btnCookie5" class="btn btn-default" Text="Refresh con postBack" runat="server" OnClick="ButtonRefresh_Click"/>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    ViewState
                </div>
                <div class="panel-body">
                    <asp:Label id="lblViewState" Text="" runat="server" />
                </div>
                <div class="panel panel-footer">
                    <asp:Button id="btnViewState" class="btn btn-default" Text="Guardar ViewState" runat="server" onClick="btnViewState_Click"/>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Variables de sesión y aplicación
                </div>
                <div class="panel-body">
                    <asp:Label id="lblVar" Text="" runat="server" />
                    <asp:Label id="lblVar2" Text="" runat="server" />
                </div>
                <div class="panel panel-footer">
                    <asp:Button id="btnVar1" class="btn btn-default" Text="Crear variable de sesión" runat="server" onClick="btnVar1_Click"/>
                    <asp:Button id="btnVar2" class="btn btn-default" Text="Crear variable de aplicación" runat="server" onClick="btnVar2_Click"/>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Ejemplo de utilización de la clase Global asax
                </div>
                <div class="panel-body">
                    <!-- esta parte falta completar, averiguar que es un .asax -->
                    <asp:Label id="lblGlobalAsax" Text="" runat="server" ></asp:Label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>