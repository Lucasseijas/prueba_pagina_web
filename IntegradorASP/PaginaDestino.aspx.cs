﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegradorASP
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //POST
            if (Request.QueryString["nombre"] == null & Request.QueryString["apellido"] == null & Context.Items["Nombre"] == null)
            {
                if (PreviousPage != null)
                {
                    TextBox txtNombre = null;
                    TextBox txtApellido = null;
                    if (PreviousPage.Master.FindControl("ContentPlaceHolder").FindControl("txtNombre") != null)
                    {
                        txtNombre = (TextBox)PreviousPage.Master.FindControl("ContentPlaceHolder").FindControl("txtNombre");
                    }
                    if (PreviousPage.Master.FindControl("ContentPlaceHolder").FindControl("txtApellido") != null)
                    {
                        txtApellido = (TextBox)PreviousPage.Master.FindControl("ContentPlaceHolder").FindControl("txtApellido");
                    }
                    if (txtNombre != null & txtApellido != null)
                    {
                        this.lblNombreCompleto.Text = this.lblNombreCompleto.Text + txtNombre.Text + " " + txtApellido.Text;
                    }
                }
            }
            //GET
            else
            {
                this.lblNombreCompleto.Text = this.lblNombreCompleto.Text + Request.QueryString["Nombre"] + " " + Request.QueryString["Apellido"];
            }
            //Context
            if(Context.Items["Nombre"] != null)
            {
                lblNombreCompleto.Text = lblNombreCompleto.Text + Context.Items["Nombre"].ToString() + " " + Context.Items["Apellido"].ToString();
            }
        }
    }
}