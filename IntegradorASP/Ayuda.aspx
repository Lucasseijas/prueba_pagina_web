﻿<%@ Page Title="Ayuda" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ayuda.aspx.cs" Inherits="IntegradorASP.Ayuda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            ayuda
        </div>
        <div class="panel-body">
                <ul>
                    <li>
                        <asp:HyperLink id="HyperlinkEstado"   NavigateUrl="~/Estado.aspx" runat="server"> Mantenimiento del estado de la pagina </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink id="HyperLinkTransferencia"    NavigateUrl="~/PaginaOrigen.aspx" runat="server" > Transferencia de datos entre páginas</asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
</asp:Content>
