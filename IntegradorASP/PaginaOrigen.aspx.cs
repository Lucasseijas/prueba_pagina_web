﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IntegradorASP
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PaginaDestino.aspx?Nombre=" + this.txtNombre.Text + "&Apellido=" + this.txtApellido.Text);
        }

        protected void btnContext_Click(object sender, EventArgs e)
        {
            if(txtNombre.Text != "" && txtApellido.Text != "")
            {
                Context.Items["Nombre"] = this.txtNombre.Text;
                Context.Items["Apellido"] = this.txtApellido.Text;
                Server.Transfer("PaginaDestino.aspx");
            }
            else
            {
                Response.Write("<script>alert('debe cargar datos en nombre y apellido')</script>");
            }
        }
    }
}