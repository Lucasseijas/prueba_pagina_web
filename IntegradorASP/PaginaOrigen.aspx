﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaginaOrigen.aspx.cs" Inherits="IntegradorASP.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="panel panel-primary">
        <div class=" panel-heading">
            Transferencia de datos entre páginas. Página origen
        </div>
        <div class="panel-body">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Transferencia a través de campos ocultos (POST) y a través de parámetros en la URL (GET)
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="txtNombre">NOMBRE </label>
                        <asp:TextBox id="txtNombre" runat="server" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label for="txtApellido">APELLIDO </label>
                        <asp:TextBox id="txtApellido" runat="server" class="form-control"/>
                    </div>
                </div>


                <div class="panel-footer">
                    <asp:Button id="btnPost" Text="POST" runat="server" PostBackUrl="~/PaginaDestino.aspx" class="btn btn-default"/>
                    <asp:Button id="btnGet" Text="GET" runat="server" class="btn btn-default" OnClick="btnGet_Click"/>
                    <asp:Button id="btnContext" Text="CONTEXT" runat="server" class="btn btn-default" OnClick="btnContext_Click"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
